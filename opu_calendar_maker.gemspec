# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'opu_calendar_maker/version'

Gem::Specification.new do |spec|
  spec.name          = "opu_calendar_maker"
  spec.version       = OPUCalendarMaker::VERSION
  spec.authors       = ["atpsaym"]
  spec.email         = ["atpsaym@gmail.com"]
  spec.description   = %q{大阪府立大学の学年暦をパースし、iCalendar形式で出力する}
  spec.summary       = %q{大阪府立大学の学年暦をパースし、iCalendar形式で出力する}
  spec.homepage      = "https://bitbucket.org/atpsaym/opu_calendar"
  spec.license       = "Public Domain"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "icalendar", "~> 2.1"
  spec.add_dependency "nokogiri"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "minitest", "~> 5.5"
end
