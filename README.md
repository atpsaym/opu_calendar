大阪府立大学の学年暦を生成するスクリプト

# 必要な環境
Ruby 2.2.1で動作確認をしています。

# 使い方
```
bundle install
wget http://www.osakafu-u.ac.jp/campus_life/schedule/index.html
bundle exec ruby bin/opu_calendar.rb index.html
```

キャンパスごとのicsファイルが生成される。

## テスト
`rake test`

# ライセンス
パブリックドメイン

# 方針
- 出力されたデータを手作業で編集するのは間違いが起こりやすいので避ける。プログラムを動かしたら完成品ができるようにする。
- 年度別にブランチを分ける。
