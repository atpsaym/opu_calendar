# -*- coding: utf-8 -*-
require 'minitest/autorun'
require 'minitest/unit'
require_relative '../lib/opu_calendar_maker'

class TestOPUCalendarMaker < MiniTest::Test
  def setup
    @ocm = OPUCalendarMaker.new
  end

  def test_normalize_table
    table = Nokogiri::HTML.fragment(<<HTML).children[0]
<table>
  <tbody>
    <tr>
      <td rowspan="2">date1</td>
      <td rowspan="2">event1</td>
      <td>campus1<br />campus2</td>
      <td>description1</td>
    </tr>
    <tr>
      <td>campus3</td>
      <td>description2</td>
    </tr>
    <tr>
      <td>date2</td>
      <td>event2</td>
      <td>campus2</td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
HTML
    @ocm.parser.normalize_table(table)
    correct_output = Nokogiri::HTML.fragment(<<HTML).children[0]
<table>
  <tbody>
    <tr>
      <td>date1</td>
      <td>event1</td>
      <td>campus1<br />campus2</td>
      <td>description1</td>
    </tr>
    <tr>
      <td>date1</td>
      <td>event1</td>
      <td>campus3</td>
      <td>description2</td>
    </tr>
    <tr>
      <td>date2</td>
      <td>event2</td>
      <td>campus2</td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
HTML
    assert_equal table.to_html.gsub(/$\s*/, ""), correct_output.to_html.gsub(/$\s*/, "")
  end

  def test_parse_date
    @ocm.parser.instance_variable_set(:@base_year, 1883)
    assert_equal @ocm.parser.parse_date("4月1日（日）"), Date.parse("1883-04-01") ... Date.parse("1883-04-02")
    assert_equal @ocm.parser.parse_date("3月1日（土）"), Date.parse("1884-03-01") ... Date.parse("1884-03-02")
    assert_equal @ocm.parser.parse_date("3月1日（土）、2日（日）"), Date.parse("1884-03-01") ... Date.parse("1884-03-03")
    assert_equal @ocm.parser.parse_date("4月10日（火）～16日（月）"), Date.parse("1883-04-10") ... Date.parse("1883-04-17")
    assert_equal @ocm.parser.parse_date("12月29日（土）～1月3日（木）"), Date.parse("1883-12-29") ... Date.parse("1884-01-04")

    @ocm.parser.instance_variable_set(:@base_year, 2013)
    assert_equal @ocm.parser.parse_date("11月1日（金）～3日（日・祝）"), Date.parse("2013-11-01") ... Date.parse("2013-11-04")

    assert_raises(ArgumentError){ @ocm.parser.parse_date("9月10日（火）～") }
  end

  def test_parse
    results = @ocm.parser.parse(Nokogiri::HTML.parse(<<HTML))
<html><body>

<div class="pagetitle">
<h1><span>平成27年度　学年暦</span></h1>
</div>

<table summary="入学式・オリエンテーション" class="tbl">
  <caption><span>入学式・オリエンテーション</span></caption>
  <thead>
    <tr>
      <th><span>日程</span></th>
      <th><span>行事</span></th>
      <th><span>備考</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span>4月1日（水）</span></td>
      <td><span>学年開始・前期開始</span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span>4月2日（木）</span></td>
      <td><span>新入生カリキュラムオリエンテーション</span><br><span>教職課程オリエンテーション</span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span>4月6日（月）</span></td>
      <td><span>入学式（大阪国際会議場）</span></td>
      <td><span>開学記念日</span></td>
    </tr>
    <tr>
      <td><span>4月9日（木</span><span>）</span></td>
      <td><span>新入生学域オリエンテーション</span></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>

<table summary="前期" class="tbl"><caption><span>前期</span></caption>
  <thead>
    <tr>
      <th><span>日程</span></th>
      <th><span>行事</span></th>
      <th><span>キャンパス</span></th>
      <th><span>備考</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span>4月9日（木）</span></td>
      <td><span>定期健康診断</span></td>
      <td><span>羽曳野</span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span>4月10日（金）</span></td>
      <td><span>前期授業開始</span></td>
      <td><span>全て</span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span>4月10日（金）～16日（木）</span></td>
      <td><span>前期受講申請期間・履修相談</span></td>
      <td><span>全て</span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span>5月7日（木）、8日（金）</span></td>
      <td><span>振替休講日</span></td>
      <td><span>全て</span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><span>5月23日（土）、24日（日）</span></td>
      <td><span>第54回友好祭</span></td>
      <td><span>全て</span></td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>

<table summary="後期" class="tbl"><caption><span>後期</span></caption>
  <thead>
    <tr>
      <th><span>日程</span></th>
      <th><span>行事</span></th>
      <th><span>キャンパス</span></th>
      <th><span>備考</span></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><span>9月19日（土）～10月5日（月）</span></td>
      <td><span>後期受講申請期間・履修相談</span></td>
      <td><span>全て</span></td>
      <td><span>履修相談は9月24日より</span></td>
    </tr>
    <tr>
      <td><span>11月1日（日）～11月3日（火）</span></td>
      <td><span>第67回白鷺祭</span></td>
      <td><span>全て</span></td>
      <td><span>11月2日休講日</span></td>
    </tr>
  </tbody>
</table>

</body></html>
HTML

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-04-01")...Date.parse("2015-04-02"),
      "学年開始・前期開始",
      nil,
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス, りんくうキャンパス, 羽曳野キャンパス"
    ), results[0]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-04-02")...Date.parse("2015-04-03"),
      "新入生カリキュラムオリエンテーション\n教職課程オリエンテーション",
      nil,
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス, りんくうキャンパス, 羽曳野キャンパス"
    ), results[1]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-04-06")...Date.parse("2015-04-07"),
      "入学式",
      "開学記念日",
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "大阪国際会議場"
    ), results[2]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-04-09")...Date.parse("2015-04-10"),
      "新入生学域オリエンテーション",
      nil,
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス, りんくうキャンパス, 羽曳野キャンパス"
    ), results[3]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-04-09")...Date.parse("2015-04-10"),
      "定期健康診断",
      nil,
      Set.new(["羽曳野"]),
      "羽曳野キャンパス"
    ), results[4]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-04-10")...Date.parse("2015-04-11"),
      "前期授業開始",
      nil,
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス, りんくうキャンパス, 羽曳野キャンパス"
    ), results[5]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-04-10")...Date.parse("2015-04-17"),
      "前期受講申請期間・履修相談",
      nil,
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス, りんくうキャンパス, 羽曳野キャンパス"
    ), results[6]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-05-07")...Date.parse("2015-05-09"),
      "振替休講日",
      nil,
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス, りんくうキャンパス, 羽曳野キャンパス"
    ), results[7]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-05-23")...Date.parse("2015-05-25"),
      "第54回友好祭",
      nil,
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス"
    ), results[8]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-09-19")...Date.parse("2015-10-06"),
      "後期受講申請期間・履修相談",
      "履修相談は9月24日より",
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス, りんくうキャンパス, 羽曳野キャンパス"
    ), results[9]

    assert_equal OPUCalendarMaker::Event.new(
      Date.parse("2015-11-01")...Date.parse("2015-11-04"),
      "第67回白鷺祭",
      "11月2日休講日",
      Set.new(["中百舌鳥", "りんくう", "羽曳野"]),
      "中百舌鳥キャンパス"
    ), results[10]
  end
end
