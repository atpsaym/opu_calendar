# -*- coding: utf-8 -*-

require 'set'
require 'date'
require 'nokogiri'
require 'icalendar'
require 'opu_calendar_maker/event'
require 'opu_calendar_maker/parser'

class Nokogiri::XML::Node
  def index
    self.parent.children.select(&:element?).index(self)
  end
end

class OPUCalendarMaker
  CAMPUSES = Set.new(["中百舌鳥", "りんくう", "羽曳野"])

  attr_reader :parser
  attr_accessor :uid_suffix

  def initialize
    @parser = Parser.new
    @uid_suffix = "@unofficial_opu_calendar"
  end

  def base_year
    @parser.base_year
  end

  def campuses_to_sym(name)
    {
      "中百舌鳥" => :nakamozu, "りんくう" => :rinku, "羽曳野" => :habikino, "全て" => :all
    }.fetch(name)
  end

  # イベントをキャンパスごとに振り分け
  def assort_events(events)
    campus_events = Hash.new{ |h, k| h[k] = [] }
    events.each do |event|
      event.campuses.each do |campus|
        campus_events[campus] << event
      end
    end

    campus_events["全て"] = events

    campus_events
  end

  def make_calendars(campus_events)
    calendars = campus_events.map do |name, events|
      calendar = Icalendar::Calendar.new

      calendar.append_custom_property("X-WR-CALNAME", "大阪府立大学 H#{base_year - 1988}年度#{name != "全て" ? "#{name}キャンパス" : "" }学年暦（非公式）")
      calendar.append_custom_property("X-WR-CALDESC", "http://www.osakafu-u.ac.jp/campus_life/schedule/index.htmlを元に作成した非公式の学年暦")

      calendar.timezone do |t|
        t.tzid           = "Asia/Tokyo"

        t.standard do |s|
          s.tzoffsetfrom = "+0900"
          s.tzoffsetto   = "+0900"
          s.tzname       = "JST"
          s.dtstart      = "19390101T000000"
        end
      end

      uid_suffix = @uid_suffix
      events.each do |event|
        calendar.event do |e|
          e.uid     = event.uid(uid_suffix)
          e.summary = event.summary.gsub("\n", "・")
          e.dtstart = Icalendar::Values::Date.new(event.dt.begin)
          e.dtend   = Icalendar::Values::Date.new(event.dt.end)
          e.description = unless event.descriptions.nil?
                            case event.descriptions
                            when String
                              event.descriptions
                            when Hash
                              event.descriptions.map do |campuses, str|
                                "(#{campuses.to_a.join(",")}: #{str})"
                              end.join(" ")
                            end
                          end
          e.location = event.location unless event.location.nil?
        end
      end

      [name, calendar]
    end

    Hash[*calendars.flatten]
  end
end
