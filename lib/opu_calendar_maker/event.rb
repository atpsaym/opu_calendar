require 'digest/md5'

class OPUCalendarMaker
  class Event < Struct.new(:dt, :summary, :descriptions, :campuses, :location)
    def uid(suffix = "")
      Digest::MD5.hexdigest([summary, dt, campuses.to_a].map(&:to_s).join) + suffix
    end
  end
end
