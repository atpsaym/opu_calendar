class OPUCalendarMaker
  class Parser

    attr_reader :base_year

    class EventNotFoundError < StandardError
    end

    def parse(page)
      @base_year = page.at_css(".pagetitle h1").text.match(/平成(\d+)年度　学年暦/)[1].to_i + 1988

      events = parse_events(page)

      events.each do |i|
        i.location = i.campuses.map{ |j| j + "キャンパス" }.join(", ")
      end

      event = events.find{ |i| i.summary == "入学式（大阪国際会議場）" }
      if event
        event.summary.sub!("（大阪国際会議場）", "")
        event.location = "大阪国際会議場"
      else
        raise EventNotFoundError, "「入学式（大阪国際会議場）」が見つかりませんでした。"
      end

      regexp = /\A第\d+回友好祭\z/
      event = events.find{ |i| i.summary =~ regexp }
      if event
        event.location = "中百舌鳥キャンパス"
      else
        raise EventNotFoundError, "#{regexp}が見つかりませんでした。"
      end

      regexp = /\A第\d+回白鷺祭\z/
      event = events.find{ |i| i.summary =~ regexp }
      if event
        event.location = "中百舌鳥キャンパス"
      else
        raise EventNotFoundError, "#{regexp}が見つかりませんでした。"
      end

      events
    end

    def parse_events(page)
      events = page.search(".tbl").flat_map do |table|
        normalize_table(table)
        table_to_hash(table)
      end

      # hash to event
      events.map! do |event|
        Event.new(*event.values_at(*%w|日程 行事 備考 キャンパス|))
      end

      events.each do |event|
        event.location = nil if event.location == ""
        event.descriptions = nil if event.descriptions == ""
      end

      # 日程をパース
      events.each{ |event| event.dt = parse_date(event.dt) }

      # campusesをSetに変換
      events.each do |event|
        if event.campuses.nil?
          event.campuses = CAMPUSES.dup
        elsif event.campuses == "全て"
          event.campuses = CAMPUSES.dup
        else
          event.campuses = Set.new(CAMPUSES.select do |campus|
                                     event.campuses.include? campus
                                   end)
        end
      end

      events
    end

    def parse_date(date_str)
      m = date_str.match(
        # "～"(U+FF5E) != "〜"(U+301C)
        /\A(\d+)月(\d+)日（.+?）(?:(?:～|、)(?:(\d+)月)?(\d+)日（.+?）)?\z/
      )
      raise ArgumentError.new("Can't parse #{date_str}") if m.nil?
      s_month, s_day, e_month, e_day = m.captures.map{|i| i.to_i if i}
      start_date = Date.new(@base_year + (s_month < 4 ? 1 : 0), s_month, s_day)
      if e_day.nil?
        start_date ... start_date + 1
      else
        e_month ||= s_month
        end_date = Date.new(@base_year + (e_month < 4 ? 1 : 0), e_month, e_day)
        start_date ... end_date + 1
      end
    end

    def normalize_table(table)
      # |-----|-----|     |-----|-----|
      # | foo | bar |     | foo | bar |
      # |     |-----| ->  |-----|-----|
      # |     | boo |     | foo | boo |
      # |-----|-----|     |-----|-----|

      tbody = table.at("tbody")
      rowspan = {}
      tbody.search("tr").each do |tr|
        rowspan.select!{ |index, v| v[:count] > 0 }
        rowspan.each do |index, v|
          tr.search("td")[index].before(v[:node].dup)
          v[:count] -= 1
        end
        td_nodes = tr.search("td")
        td_nodes.each do |td|
          rowspan_value = td["rowspan"].to_i
          if rowspan_value > 1
            rowspan[td.index] = {count: rowspan_value - 1, node: td}
            td.remove_attribute("rowspan")
          end
        end
      end
    end

    def table_to_hash(table)
      head = table.search("thead > tr > th").map(&:text)
      table.search("tbody > tr").map{ |i| i.search("td")}.select do |i|
        i.size == head.size
      end.map do |row|
        values = row.map do |td|
          td.children.map do |child|
            if child.name == "br"
              "\n"
            else
              # "&nbsp;"も削除する
              child.text.gsub(/\A[[:space:]]+|[[:space:]]+\z/, "")
            end
          end.join("")
        end

        Hash[head.zip(values)]
      end
    end
  end
end
